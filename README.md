# Automatización del Aumento de Cupo #

En este repositorio se almacenan los codigos utilizados en el desarrollo de la automatización del aumento de cupo para el producto Rapiflex y clientes Recurrentes.

## ¿Para qué es este repositorio? ##

* Contiene análisis de NPL de los Aumentos de Cupo Historico realizados manualmente (aumento_cupo_npl.ipynb).
* Contiene el desarrollo del modelo de machine learning para la aprobación de aumento de cupo (aumento_cupo_npl.ipynb) en python 3.9.7.
* Contiene análisis de la politica de asignacion de cupo con base al score desarrolla (analisis_asignacion_cupo.ipynb).
* Contiene la validación del modelo de aprobación y asignacion de cupo, comparando la calificación con el proceso manual y el conocimiento y experiencia del evaluador, para aliniar este nuevo proceso con el negocio (calificacion_aprobados_ysa.ipynb).
* Contiene el análisis de conversión de Solicitudes de los Aumentos de Cupo Historico realizados manualmente, para tener un estimado de los días y la tasa de conversión del aumento (conversion_solicitudes_aumento_cupo.ipynb).
* Contiene el codigo necesario para realizar el proceso de aprobación y asignacion de cupo para los clientes de Rapiflex y recurrentes que cumplen son ciertas caracteristicas (calificacion_recurrentes.ipynb).

## Ejecución del Proceso ##

Para ejecutar satisfactoriamente este proceso (calificacion_recurrentes.ipynb):

* Debe tener permisos a la base de datos en producción
* Debe tener instalado python 3.9.7
* Debe tener instaladas las librerias del archivo "..\Modelo\requeriments_modelo.txt"
* Debe analizar 2 salidas de proceso (log_predicciones_@fecha@.xlsx y Aumento_Cupo_@fecha@.csv).